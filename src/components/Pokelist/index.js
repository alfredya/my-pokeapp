import React from 'react';
import axios from 'axios';

class Pokelist extends React.Component {
  
  constructor() {
    super();
    this.state = { pokemons: [], selectedPokemon: null };
  }

  componentDidMount() {
    axios.get('https://pokeapi.co/api/v2/pokemon').then((response) => {
      this.setState({ pokemons: response.data.results });
    });
  }

  selectPokemon(e, pokemon) {
    e.preventDefault();
    axios.get(pokemon.url).then((response) => {
      console.log(response)
      this.setState({ selectedPokemon: response.data });
    });
  }

  renderPokemons() {
    return this.state.pokemons.map((pokemon) => {
      return (
        <div>
          <a href="" onClick={(e) => this.selectPokemon(e, pokemon) }>{ pokemon.name }</a>
        </div>
      );
    });
  }

  render() {
    return (
      <div>
        <h1>My Pokelist</h1>
        { this.renderPokemons() }
        <div>
          { this.state.selectedPokemon && 
            <img style={{ width: '200px' }} src={this.state.selectedPokemon.sprites.front_default} />
          }
        </div>
      </div>
    );
  }
}

export default Pokelist;
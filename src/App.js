import React from 'react';
import Pokelist from './components/Pokelist';
import './App.css';

function App() {
  return (
    <div className="App">
      <Pokelist name="Finnovate.io"/>
    </div>
  );
}

export default App;
